import XCTest

class iOSTestUITests: XCTestCase {
    override func setUp() {
        continueAfterFailure = false
    }


    func test_visitEachScreen() {
        let app = XCUIApplication()
        app.launch()
        
        XCTAssert(app.staticTexts["Coding Tasks"].exists, "On Main Menu")
        
        app.buttons["CHAT"].tap()
        
        XCTAssert(app.staticTexts["Chat"].exists, "On Chat")
        
        app.navigationBars.buttons.element(boundBy: 0).tap()
        
        app.buttons["LOGIN"].tap()
        
        XCTAssert(app.staticTexts["Login"].exists, "On Login")
        
        app.navigationBars.buttons.element(boundBy: 0).tap()
        
        app.buttons["ANIMATION"].tap()
        
        XCTAssert(app.staticTexts["Animation"].exists, "On animation")

    }

}
