import UIKit

class TextFieldMethods {
    func indent(by indent: Int, textField: UITextField) -> UITextField {
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: indent, height: 0))
        textField.leftViewMode = UITextField.ViewMode.always
        return textField
    }
    
}
