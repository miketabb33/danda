import UIKit

class CustomUIAlert {
    func newAlert(title: String, message: String, onClick: (() -> ())?) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) { _ in
            if let safeOnClick = onClick {
                safeOnClick()
            }
        }
        
        alertController.addAction(ok)
        return alertController
    }
}
