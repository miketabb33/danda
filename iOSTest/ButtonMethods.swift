import UIKit

class ButtonMethods {
    func setUpButton(_ button: UIButton) -> UIButton {
        let capitalizedTitle = button.titleLabel?.text?.uppercased()
        button.setTitle(capitalizedTitle, for: .normal)
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 0);
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 22 + 16, bottom: 0, right: 0)
        return button
    }
}
