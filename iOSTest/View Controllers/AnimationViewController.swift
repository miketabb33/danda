import UIKit
import AVFoundation

class AnimationViewController: UIViewController {
    
    /**
     * =========================================================================================
     * INSTRUCTIONS
     * =========================================================================================
     * 1) Make the UI look like it does in the mock-up.
     *
     * 2) Logo should fade out or fade in when the user hits the Fade In or Fade Out button
     *
     * 3) User should be able to drag the logo around the screen with his/her fingers
     *
     * 4) Add a bonus to make yourself stick out. Music, color, fireworks, explosions!!! Have Swift experience? Why not write the Animation 
     *    section in Swfit to show off your skills. Anything your heart desires!
     *
     **/
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var birdView: UIImageView!
    
    var player = AVAudioPlayer()
    var buttonClickManager: ButtonClickManager?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Animation"
        
        logoView.alpha = 0
        birdView.alpha = 0
        
        logoView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: 160)
        
        let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(dragging(sender:)))
        logoView.isUserInteractionEnabled = true
        logoView.addGestureRecognizer(dragGesture)
        
        configurePlayer()
        
        buttonClickManager = ButtonClickManager(logoView: logoView, button: button, player: player, birdView: birdView)
    }
    
    
    @IBAction func didPressFade(_ sender: Any) {
        buttonClickManager!.toggle()
    }
    
    @objc func dragging(sender: UIPanGestureRecognizer) {
        logoView.center = sender.location(in: view)
    }
    
    func configurePlayer() {
        let soundPath = Bundle.main.path(forResource: "hurray", ofType: "mp3")
        do {
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: soundPath!))
        } catch {
            print(error)
        }
    }
    
}
