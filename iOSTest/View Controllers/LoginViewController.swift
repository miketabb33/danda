import UIKit

class LoginViewController: UIViewController {
    
    /**
     * =========================================================================================
     * INSTRUCTIONS
     * =========================================================================================
     * 1) Make the UI look like it does in the mock-up.
     *
     * 2) Take username and password input from the user using UITextFields
     *
     * 3) Using the following endpoint, make a request to login
     *    URL: http://dev.datechnologies.co/Tests/scripts/login.php
     *    Parameter One: email
     *    Parameter Two: password
     *
     * 4) A valid email is 'info@datechnologies.co'
     *    A valid password is 'Test123'
     *
     * 5) Calculate how long the API call took in milliseconds
     *
     * 6) If the response is an error display the error in a UIAlertView
     *
     * 7) If the response is successful display the success message AND how long the API call took in milliseconds in a UIAlertView
     *
     * 8) When login is successful, tapping 'OK' in the UIAlertView should bring you back to the main menu.
     **/
    
    // MARK: - Properties
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var loginManager: LoginManager?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login"
        
        loginManager = LoginManager(parentVC: self)
        
        usernameField = TextFieldMethods().indent(by: 24, textField: usernameField)
        passwordField = TextFieldMethods().indent(by: 24, textField: passwordField)
    }
    
    @IBAction func didPressLoginButton(_ sender: Any) {
        LoginClient().login(withUsername: usernameField.text!, password: passwordField.text!, completion: loginManager?.responseHandler(result:))
    }
}
