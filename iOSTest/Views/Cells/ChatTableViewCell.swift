import UIKit

class ChatTableViewCell: UITableViewCell {
    
    /**
     * =========================================================================================
     * INSTRUCTIONS
     * =========================================================================================
     * 1) Setup cell to match mockup
     *
     * 2) Include user's avatar image
     **/
    
    // MARK: - Outlets
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var bodyContainer: UIView!
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Public
    func setCellData(message: ChatMessage) {
        header.text = message.username
        body.text = message.text
        avatar.image = UIImage(data: message.imageData)
    }
    
    func setCellAttributes() {
        setBodyContainerAttributes()
        setImageAttributes()
    }
    
    func setBodyContainerAttributes() {
        bodyContainer.layer.masksToBounds = true
        bodyContainer.layer.cornerRadius = 8
        bodyContainer.layer.borderWidth = 1
        bodyContainer.layer.borderColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1).cgColor
    }
    
    func setImageAttributes() {
        avatar.layer.cornerRadius = avatar.bounds.width/2
    }
}
