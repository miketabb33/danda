import UIKit

class FetchChatCompletion {
    
    var parentVC: ChatViewController
    
    init(parentVC: ChatViewController) {
        self.parentVC = parentVC
    }
    
    func success(messages: [Message]?) {
        DispatchQueue.main.async {
            if let safeMessages = messages {
                for message in safeMessages {
                    let imageData = NSData(contentsOf: message.avatarURL)!
                    self.parentVC.messages.append(ChatMessage(username: message.username, text: message.text, imageData: imageData as Data))
                }
                self.updateTableView()
            }
        }
    }

    func error(error: String?) {
        var alert = UIAlertController()
        
        if let safeError = error {
            alert = CustomUIAlert().newAlert(title: "Error", message: safeError, onClick: nil)
        } else {
            alert = CustomUIAlert().newAlert(title: "Error", message: "Unknown error", onClick: nil)
        }
        
        parentVC.present(alert, animated: true)
    }
    
    func updateTableView() {
        parentVC.activityIndicator.stopAnimating()
        parentVC.chatTable.rowHeight = UITableView.automaticDimension
        parentVC.chatTable.estimatedRowHeight = 120
        parentVC.chatTable.reloadData()
    }
}


