import Foundation

struct ChatMessage {
    let username: String
    let text: String
    let imageData: Data
}
