import UIKit
import AVFoundation

class ButtonClickManager {
    
    var logoView: UIImageView
    var button: UIButton
    var player: AVAudioPlayer
    var birdView: UIImageView
    
    var birdFrame = 0
    
    var timer = Timer()
    
    init(logoView: UIImageView, button: UIButton, player: AVAudioPlayer, birdView: UIImageView) {
        self.logoView = logoView
        self.button = button
        self.player = player
        self.birdView = birdView
        
        logoView.layer.zPosition = 1
    }
    
    func toggle() {
        if logoView.alpha == 0 {
            player.play()
            timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(birdAnimation), userInfo: nil, repeats: true)
            button.setTitle("FADE OUT", for: .normal)
            fade(logoView, alpha: 1, completion: nil)
            fade(birdView, alpha: 1, completion: nil)
        } else {
            button.setTitle("FADE IN", for: .normal)
            fade(logoView, alpha: 0, completion: nil)
            fade(birdView, alpha: 0, completion: timer.invalidate)
        }
    }
    
    func fade(_ view: UIView, alpha: CGFloat, completion: (()->())?) {
        UIView.animate(withDuration: 0.5, animations: {
            view.alpha = alpha
        }) { _ in
            if let safeCompletion = completion {
                safeCompletion()
            }
        }
    }

    @objc func birdAnimation() {
        birdView.image = UIImage(named: "bird_\(birdFrame).gif")
        birdFrame += 1
        if birdFrame >= 9 {
            birdFrame = 0
        }
    }
}
