import UIKit

class LoginManager {
    let parentVC: LoginViewController
    
    init(parentVC: LoginViewController) {
        self.parentVC = parentVC
    }
    
    func responseHandler(result: [AnyHashable : Any]?) {
        let code = result!["code"]! as! String
        let message = result!["message"]! as! String
        let time = result!["time"]! as! String
        
        switch code {
        case "Error":
            presentAlert(title: "Login Failed", message: "The email/password combination is incorrect", onClick: nil)
        case "FetchError":
            presentAlert(title: "Error", message: message, onClick: nil)
        case "Success":
            presentAlert(title: "Login Successful", message: "It took \(time) milliseconds to log in", onClick: self.successfulLogin)
        default:
            presentAlert(title: "Unknown Error", message: "Please try again later", onClick: nil)
        }
    }
    
    func presentAlert(title: String, message: String, onClick: (() -> ())?) {
        DispatchQueue.main.async {
            let alert = CustomUIAlert().newAlert(title: title, message: message, onClick: onClick)
            self.parentVC.present(alert, animated: true)
        }
    }
    
    func successfulLogin() {
        parentVC.navigationController?.popViewController(animated: true)
    }
}
