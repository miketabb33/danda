#import <Foundation/Foundation.h>

@interface LoginClient : NSObject
- (void)loginWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(NSDictionary *))completion;
@end
