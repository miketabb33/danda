#import "ChatClient.h"

@interface ChatClient ()
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation ChatClient

/**
 * =========================================================================================
 * INSTRUCTIONS
 * =========================================================================================
 * 1) Make a request to fetch chat data used in this app.
 *
 * 2) Using the following endpoint, make a request to fetch data
 *    URL: http://dev.datechnologies.co/Tests/scripts/chat_log.php
 **/

- (void)fetchChatData:(void (^)(NSArray<Message *> *))completion withError:(void (^)(NSString *error))errorBlock
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    _session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL * url = [NSURL URLWithString:@"http://dev.datechnologies.co/Tests/scripts/chat_log.php"];
    
    NSURLSessionDataTask *dataTask = [self createDataTaskWithURL:url completion:completion withError:errorBlock];
    
    [dataTask resume];
}

- (NSURLSessionDataTask *)createDataTaskWithURL:(NSURL *)url completion:(void (^)(NSArray<Message *> *))completion withError:(void (^)(NSString *error))errorBlock
{
    NSURLSessionDataTask *dataTask = [_session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error != nil) {
            [self errorHandlerFor:error response:response withError:errorBlock];
        } else {
            NSError *jsonError;
            NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            [self completionHandlerFor:results jsonError:jsonError completion:completion];
        }
    }];
    
    return dataTask;
}

- (void)errorHandlerFor:(NSError *)error response:(NSURLResponse *)response withError:(void (^)(NSString *error))errorBlock
{
    NSString *localizedDescription = [error localizedDescription];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    int statusCode = (int)[httpResponse statusCode];
    
    NSString *errorMessage = [NSString stringWithFormat:@"%@ \n Response status code: %d", localizedDescription, statusCode];
    
    errorBlock(errorMessage);
}

- (void)completionHandlerFor: (NSDictionary *)results jsonError:(NSError *)jsonError completion:(void (^)(NSArray<Message *> *))completion
{
    if (jsonError != nil) {
        [NSException raise:@"JSONParsingError" format:@"Error in parsing chat data to NS Dictionary"];
    } else {
        NSMutableArray<Message *> *messages = [self buildMessagesWith:results];
        completion(messages);
    }
}

- (NSMutableArray<Message *> *)buildMessagesWith:(NSDictionary *)results
{
    NSMutableArray<Message *> *messages = NSMutableArray.new;
    for (NSDictionary *result in results[@"data"]) {
        Message *message = [[Message alloc] initWithDictionary:result];
        [messages addObject:message];
    }
    return messages;
}

@end
