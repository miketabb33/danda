#import "LoginClient.h"

@interface LoginClient ()
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation LoginClient

/**
 * =========================================================================================
 * INSTRUCTIONS
 * =========================================================================================
 * 1) Make a request here to login.
 *
 * 2) Using the following endpoint, make a request to login
 *    URL: http://dev.datechnologies.co/Tests/scripts/login.php
 *
 * 3) Don't forget, the endpoint takes two parameters 'username' and 'password'
 *
 * 4) A valid email is 'info@datechnologies.co'
 *    A valid password is 'Test123'
 **/

- (void)loginWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(NSDictionary *))completion
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    _session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSString *urlString = [NSString stringWithFormat:@"http://dev.datechnologies.co/Tests/scripts/login.php?email=%@&password=%@", username, password];
    
    NSURL * url = [NSURL URLWithString:urlString];
    
    NSURLSessionDataTask *dataTask = [self createDataTaskWithURL:url completion:completion];
    
    [dataTask resume];
}

- (NSURLSessionDataTask *)createDataTaskWithURL:(NSURL *)url completion:(void (^)(NSDictionary *))completion
{
    __block int responseTime = 0;
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:.01 repeats:true block:^(NSTimer * _Nonnull timer) {
        responseTime += 10;
    }];
    
    NSURLSessionDataTask *dataTask = [_session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [timer invalidate];
        
        if (error != nil) {
            NSDictionary *response = [self newResponseCode:@"FetchError" message:[error localizedDescription] time:responseTime];
            completion(response);
        } else {
            NSError *jsonError;
            NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            [self parsingHandlerFor:results jsonError:jsonError responseTime:responseTime completion:completion];
        }
    }];
    
    return dataTask;
}

- (void) parsingHandlerFor: (NSDictionary *)results jsonError:(NSError *)jsonError responseTime:(int)responseTime completion:(void (^)(NSDictionary *))completion
{
    if (jsonError != nil) {
        [NSException raise:@"JSONParsingError" format:@"Log in response was not properly parsed into an NSDictionary"];
    } else {
        NSString *code = [NSString stringWithFormat:@"%@", results[@"code"]];
        NSString *message = [NSString stringWithFormat:@"%@", results[@"message"]];
        
        NSDictionary *response = [self newResponseCode:code message:message time:responseTime];
        completion(response);
    }
}

- (NSDictionary *) newResponseCode:(NSString *)code message:(NSString *)message time:(int)time
{
    return @{ @"code" : code, @"message" : message, @"time" : [NSString stringWithFormat:@"%d", time] };
}

@end
