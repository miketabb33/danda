#import <Foundation/Foundation.h>
#import "Message.h"

@interface ChatClient : NSObject
- (void)fetchChatData:(void (^)(NSArray<Message *> *))completion withError:(void (^)(NSString *error))errorBlock;
- (NSURLSessionDataTask *)createDataTaskWithURL:(NSURL *)url completion:(void (^)(NSArray<Message *> *))completion withError:(void (^)(NSString *error))errorBlock;
- (void)errorHandlerFor:(NSError *)error response:(NSURLResponse *)response withError:(void (^)(NSString *error))errorBlock;
- (void)completionHandlerFor: (NSDictionary *)results jsonError:(NSError *)jsonError completion:(void (^)(NSArray<Message *> *))completion;
- (NSMutableArray<Message *> *)buildMessagesWith:(NSDictionary *)results;
@end
