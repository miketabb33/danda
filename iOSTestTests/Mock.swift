import Foundation

class Mock {
    func chatDictionaryData() -> [String : [[String : String]]] {
        return ["data" : [data1, data2, data3]]
    }
    
    let data1: [String : String] = [
        "user_id" : "1",
        "name" : "Testy",
        "avatar_url" : "Test1",
        "message" : "This is a message"
    ]
    
    let data2: [String : String] = [
        "user_id" : "2",
        "name" : "Testy McGee",
        "avatar_url" : "Test2",
        "message" : "This is another message"
    ]
    
    let data3: [String : String] = [
        "user_id" : "3",
        "name" : "Testy Sr",
        "avatar_url" : "Test3",
        "message" : "This is yet another message"
    ]
}
