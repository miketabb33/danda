import XCTest
@testable import iOSTest

class ChatClientTests: XCTestCase {
    
    let chatClient = ChatClient()

    func test_buildMessage() {
        let data = Mock().chatDictionaryData()
        
        let messages = chatClient.buildMessages(with: data) as! [Message]
        
        XCTAssert(data["data"]!.count == messages.count, "There shoud be as many messages as there are data items")
        
        XCTAssert(data["data"]![0]["name"] == messages[0].username, "The data should contain the same info as the message model")
        XCTAssert(URL(string: data["data"]![0]["avatar_url"]!) == messages[0].avatarURL, "The data should contain the same info as the message model")
        XCTAssert(data["data"]![0]["message"] == messages[0].text, "The data should contain the same info as the message model")
        XCTAssert(data["data"]![0]["user_id"] == messages[0].userID, "The data should contain the same info as the message model")
        
        
        XCTAssert(data["data"]![1]["name"] == messages[1].username, "The data should contain the same info as the message model")
        XCTAssert(URL(string: data["data"]![1]["avatar_url"]!) == messages[1].avatarURL, "The data should contain the same info as the message model")
        XCTAssert(data["data"]![1]["message"] == messages[1].text, "The data should contain the same info as the message model")
        XCTAssert(data["data"]![1]["user_id"] == messages[1].userID, "The data should contain the same info as the message model")
        
        
        XCTAssert(data["data"]![2]["name"] == messages[2].username, "The data should contain the same info as the message model")
        XCTAssert(URL(string: data["data"]![2]["avatar_url"]!) == messages[2].avatarURL, "The data should contain the same info as the message model")
        XCTAssert(data["data"]![2]["message"] == messages[2].text, "The data should contain the same info as the message model")
        XCTAssert(data["data"]![2]["user_id"] == messages[2].userID, "The data should contain the same info as the message model")
    }

}
