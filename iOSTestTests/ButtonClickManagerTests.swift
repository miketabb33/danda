import XCTest
import AVFoundation
@testable import iOSTest

class ButtonClickManagerTests: XCTestCase {
    let logoView = UIImageView()
    let birdView = UIImageView()
    let player = AVAudioPlayer()
    let button = UIButton()
    
    var buttonClickManager: ButtonClickManager?
    
    override func setUp() {
        buttonClickManager = ButtonClickManager(logoView: logoView, button: button, player: player, birdView: birdView)
    }
    
    
    func test_toggle_fadeIn() {
        buttonClickManager!.logoView.alpha = 0
        buttonClickManager!.birdView.alpha = 0
        
        buttonClickManager!.toggle()
        
        XCTAssert(buttonClickManager!.logoView.alpha == 1, "Given a logo alpha value of 0, the method should set the logo alpha value to 1")
        XCTAssert(buttonClickManager!.birdView.alpha == 1, "Given a logo alpha value of 0, the method should set the bird view alpha value to 1")
        XCTAssert(buttonClickManager!.button.titleLabel?.text == "FADE OUT", "Method should change button title text to 'Fade Out'")
    }
    
    
    func test_toggle_fadeOut() {
        buttonClickManager!.logoView.alpha = 1
        buttonClickManager!.birdView.alpha = 1
        
        buttonClickManager!.toggle()
        
        XCTAssert(buttonClickManager!.logoView.alpha == 0, "Given a logo alpha value of 1, the method should set the logo alpha value to 0")
        XCTAssert(buttonClickManager!.birdView.alpha == 0, "Given a logo alpha value of 1, the method should set the bird view alpha value to 0")
        XCTAssert(buttonClickManager!.button.titleLabel?.text == "FADE IN", "Method should change button title text to 'Fade In'")
    }
    
    
    
    func test_birdAnimation_increment() {
        XCTAssert(buttonClickManager!.birdFrame == 0, "Bird frame should be at 0")
        buttonClickManager!.birdAnimation()
        XCTAssert(buttonClickManager!.birdFrame == 1, "Bird frame should be at 1")
        buttonClickManager!.birdAnimation()
        XCTAssert(buttonClickManager!.birdFrame == 2, "Bird frame should be at 2")
    }
    
    func test_birdAnimation_recycle() {
        buttonClickManager?.birdFrame = 9
        XCTAssert(buttonClickManager!.birdFrame == 9, "Bird frame should be at 9")
        buttonClickManager!.birdAnimation()
        XCTAssert(buttonClickManager!.birdFrame == 0, "Bird frame should be at 0,")
    }

}
