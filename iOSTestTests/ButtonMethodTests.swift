import XCTest
@testable import iOSTest

class ButtonMethodTests: XCTestCase {
    let buttonMethods = ButtonMethods()
    
    func test_setUpButton_() {
        var button = UIButton()
        
        button.setTitle("test", for: .normal)
        
        button = ButtonMethods().setUpButton(button)
        
        XCTAssert(button.titleLabel?.text == "TEST", "Method should capitalize button label")
        XCTAssert(button.titleEdgeInsets.left == 38, "Title should have a left edge inset equal to wireframe specs")
        XCTAssert(button.imageEdgeInsets.left == 22, "Image should have a left edge inset equal to wireframe specs")
        
    }
}
