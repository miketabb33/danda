import XCTest
@testable import iOSTest

class TextFieldMethodsTests: XCTestCase {
    let textFieldMethods = TextFieldMethods()
    
    var textField = UITextField()
    
    override func setUp() {
        textField = UITextField()
    }
    
    func test_indent() {
        textField = textFieldMethods.indent(by: 30, textField: textField)
        XCTAssert(textField.leftView?.frame.width == 30, "Method shoud add a left view with width value equal to width argument")
    }
}
